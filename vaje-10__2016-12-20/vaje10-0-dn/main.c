#include <stdio.h>
/* Domaca naloga : 
 * napisite funkcijo, 
 * ki iz datoteke, (ime datoteke je argument funkcije) 
 * prebere tabelo 3 struktur tocka v ravnini (celo stevilske koordinate)
 * in vrednosti zapise v tabelo, ki je podana kot argument funkcije.
 * Primer datoteke : 
1 2
3 4
5 6
 * Tocke so v vrsticah, x,y sta locena s presledkom.
 * Primer klica funkcije : 
 * void mojaFunkcija( char * imeDatoteke, struct tocka * tabelaTock, int velikostTabeleTock );
 *
 * Delna domaca naloga : 
 * Namesto v tabelo struktur, lahko tocke preberes v 2D tabelo.
 */
int main(int argc, char **argv)
{
	printf("hello world\n");
	return 0;
}
