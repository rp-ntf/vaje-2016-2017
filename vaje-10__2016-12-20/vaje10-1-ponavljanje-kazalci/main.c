#include <stdio.h>

/* Naloge : 
 * Naredite funkcijo, ki ciklično zamenja 3 cela števila ( a->b, b->c, c->a ) 
 * podana v funkcijo preko kazalcev.
 * Naredite funkcijo, ki preko kazalcev napolni tabelo 
 * celih stevil dolzine n z zaporednimi sodimi stevili.
 * Tabela in n sta podana kot argumenta
 * Naredite funkcijo, ki vrne povprecno vrednost tabele, 
 * in standardno deviacijo od povprecja preko kazalcev (vse so realna stevila).

 * Naredite funkcijo, ki vrne najvecje in najmanjse sodo in 
 * liho stevilo v tabeli celih stevil. 
 * Tabela celih stevil in njena dolzina sta podane kot argumenta.
 * Naredite funkcijo, ki zamenja vse elemente dveh tabel enake dolzine. 
 * Naredite funkcijo, ki deluje enako kot strcmp( char *, char *)
 * Naredite funkcijo, ki obrne niz, podan kot argument
 * Naredite funkcijo, ki vrne vsoto in vsoto kvadratkov elementov 
 * tabele (cela stevila)
 */
void ciklicnoZamenjajTriStevila( 
	int * kazalecA, int * kazalecB, int * kazalecC	)
{
	int x;
	x = *kazalecA;
	*kazalecA = *kazalecB;
	*kazalecB = *kazalecC;
	*kazalecC = x;
}
/* Naredite funkcijo, ki preko kazalcev napolni tabelo 
 * celih stevil dolzine n z zaporednimi sodimi stevili.
 */
void napolniTabelo( int * tabela, int n )
{
	int i;
	for( i=0; i<n; i++ )
		tabela[i] = 2*i;
}
/* * Naredite funkcijo, ki vrne povprecno vrednost tabele, 
 * in standardno deviacijo od povprecja preko kazalcev (vse so realna stevila).
 */
void povprecjeInDeviacija( 
	float * tabela, int n, 
	float * povprecje, float * stddev )
{
	int i;
	float vsota =0, deviacija = 0;
	for( i=0; i<n; ++i )
		vsota += tabela[i];
	*povprecje = vsota / n;
	for( i=0; i<n; ++i )
	{
		deviacija = deviacija + (*povprecje-tabela[i])*(*povprecje-tabela[i]);
	}
	*stddev = deviacija / n;
}
 /* Naredite funkcijo, ki vrne najvecje in najmanjse sodo in 
 * liho stevilo v tabeli celih stevil. */
void vrniMinMaxSodoLiho(
	int * tabela, int n,
	int * najvecjeSodo, int * najvecjeLiho, 
	int * najmanjseSodo, int * najmanjseLiho
	)
{
	int i;
	for( i=0; i<n; ++i )
	{
		if( tabela[i] % 2 == 0 )
		{
			// Soda stevila
			if( tabela[i] > *najvecjeSodo )
				*najvecjeSodo = tabela[i];
			if( tabela[i] < *najmanjseSodo )
				*najmanjseSodo = tabela[i];
		}
		else 
		{
			// Liha stevila
			if( tabela[i] > *najvecjeLiho )
				*najvecjeLiho = tabela[i];
			if( tabela[i] < *najmanjseLiho )
				*najmanjseLiho = tabela[i];
		}
	}
}


int main(int argc, char **argv)
{
	int i;
	int a = 1, b=2, c=3;
	int * kazalec, navadnaSpremenljivka;
	kazalec = & a;
	navadnaSpremenljivka = a;
	printf("Kazalec : %d -> %d\n", kazalec, *kazalec );
	printf("Navadna spremenljivka : %d\n", navadnaSpremenljivka );
	a = 10;
	printf("Kazalec : %d -> %d\n", kazalec, *kazalec );
	printf("Navadna spremenljivka : %d\n", navadnaSpremenljivka );
	int tabela[10];
	printf("Tabela : %d : %d \n", tabela, tabela[0] );
	kazalec = &tabela[0];
	printf("Kazalec : %d -> %d\n", kazalec, *kazalec );
	
	printf("Tri stevila so : %d, %d, %d\n", a, b, c );
	ciklicnoZamenjajTriStevila( &a, &b, &c );
	printf("Tri stevila so : %d, %d, %d\n", a, b, c );
	
	napolniTabelo(tabela, 10);
	for( i=0; i<10; ++i )
	{
		printf("Tabela %d = %d\n", i, tabela[i] );
	}
	
	float tabelaFl[4] = { 1.0, 2.0, 3.0, 4.0 };
	float povprecje, deviacija; 
	povprecjeInDeviacija( tabelaFl, 4, &povprecje, &deviacija );
	printf("Povprecje = %f, deviacija = %f\n", povprecje, deviacija );
	
	int tabela2[5] = { 1, 2, 10, 50, 3 };
	int vs, vl, ms, ml;
	vs = 0; vl = 0;
	ms = 100; ml = 100;
	vrniMinMaxSodoLiho( tabela2, 5, &vs, &vl, &ms, &ml );
	printf("Najvecja / najmanjsa soda / liha stevila so : \n");
	printf("Soda : %d | %d \n", vs, ms );
	printf("Liha : %d | %d \n", ml, vl );
	vs = 100; vl = 100;
	ms = 0; ml = 0;
	vrniMinMaxSodoLiho( tabela2, 5, &vs, &vl, &ms, &ml );
	printf("najmanjsa / najvecja soda / liha stevila so : \n");
	printf("Soda : %d | %d \n", ms, vs );
	printf("Liha : %d | %d \n", ml, vl );
	return 0;
}
