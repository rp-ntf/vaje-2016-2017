#include <stdio.h>

int main(int argc, char **argv)
{
	/* V programskem jeziku C lahko pisemo in beremo iz datotek na disku, 
	 * s pomocjo funkcij, ki so zelo podobne ze poznanim funkcijam 
	 * printf(...) in scanf(...).
	 * 
	 * Primer : zapisimo prvih 10 stevil v datoteko "stevila.txt"
	 * Najprej moramo datoteko odpreti : */
	FILE * f = fopen("stevila.txt", "wt" );
	/* S tem smo odprli datoteko "stevila.txt" za pisanje w=write=pisi 
	 * kot tekstovno datoteko t=text=tekst.
	 * FILE * je kazalec na datoteko. Z datotekami operiramo 
	 * preko kazalca na datoteko.
	 * 1. argument fopen je ime datoteke, 
	 * lahko je polna pot do datoteke na disku ("C:\\neki\\spet\\test.txt")
	 * ali pa relativna pot glede na lokacijo, 
	 * kjer se program izvaja. Ce podamo polno pot, 
	 * se moramo zavedati, da so znaki "\" posebni 
	 * in jih moramo zapisati z dvema \\.
	 * 2. argument datoteke pa je nacin odpiranja datoteke : 
	 * w - psi 
	 * r - beri
	 * a - dodajaj
	 * t - teks
	 * b - binarna datoteka
	 * + - odpremo v nacinu pisanja in branja
	 * Vec nacinov lahko kombiniramo skupaj ("wb", "wt", "ab", "w+"-pisi in beri). */
	if( f == NULL ) 
	{
		printf("Napaka pri odpiranju datoteke, koncujem program.\n");
		return 0;
	}
	/* Lahko se zgodi, da nam ne uspe odpreti datoteke (npr : zelimo prebrati datoteko, ki ne obstaja).
	 * V tem primeru je kazalec nastavljen na nic (NULL) in je najbolje ce uporabnika o tem obvestimo in prenehamo s programom. */
	int i;
	for( i=0; i<10; ++i )
		fprintf( f, "%d ", i );
		// printf( "%d ", i );
	/* Pisanje v datoteko je enako pisanju na zaslon, le da moramo uporabiti fprintf namesto printf ("file printf"), 
	 * in kot prvi argument podati datoteko. */
	fclose(f);
	/* Po koncanju pisanja pa moramo datoteko zapreti. Sele po zapiranju se podatki zares zapisejo na disk. */
	/* Podatke lahko potem prebermo nazaj iz datoteke : */
	FILE * f2 = fopen("stevila.txt", "rt" ); // Tokrat datoteko odpiramo za branje.
	int n;
	for( i=0; i<10; ++i )
	{
		fscanf( f2, "%d", &n );
		// scanf( "%d", &n ) ;
		printf("Prebrali smo stevilo %d\n", n );
	}
	fclose( f2 );
	return 0;
}
