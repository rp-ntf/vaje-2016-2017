#include <stdio.h>
/* * Napisite funkcijo, ki v datoteko 
 * zapise prvih 100 zaporednih stevil po vrsticah */
void zapisiStoStevil( char * imeDatoteke )
{
	FILE * f = fopen( imeDatoteke, "wt" ); // "rt" je beri tekst
	if ( f == NULL )
	{
		printf("Napaka pri odpiranju datoteke\n");
		return;
	}
	int i;
	for( i=0; i<100; ++i )
		fprintf( f, "%d\n", i ); // fscanf( ... )
	fflush(f); // Prisili zapis na disk
	fclose(f);
}

void preberiStevila( char * imeDatoteke )
{
	FILE * f = fopen( imeDatoteke, "rt" );
	if ( f == NULL )
	{
		printf("Napaka pri odpiranju datoteke\n");
		return;
	}
	int i,n;
	for( i=0; 
		fscanf( f, "%d\n", &n ) != EOF; 
		++i )
	{
		 // fscanf( ... )
		printf("Vrstica %d : %d\n", i, n );
	}
	fclose(f);
}

int main(int argc, char **argv)
{
	zapisiStoStevil( "stoStevil.txt" );
	preberiStevila( "stoStevil.txt" );
	return 0;
}
/* Naloge :
 * Napisite funkcijo, ki iz datoteke prebere 1 
 * celo stevilo iz vsake vrstice
 * Napisite funkcijo, ki iz datoteke, kjer so v 
 * vsaki vrstici napisani zneski (float), 
 * izracuna vstoto. Datoteka :
-10.2 pivo
-11.8 darilo
+250 stipendija
-100 najemnina
 * Napisite funkcijo, ki v datoteko zapise prvih 100 sodih stevil v 1. stolpec, prvih 100 lihih stevil v 2. stolpec : 
2 1
4 3
6 5
....
 * Napisite funkcijo, ki prebere 1D tabelo iz datoteke (stevila so v vrsticah) v tabelo. Argument funkcije : tabela, najvecja dolzina tabele
 */ 
