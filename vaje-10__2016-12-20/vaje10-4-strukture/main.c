#include <stdio.h>

/* Strukture 
 * V programskem jeziku C lahko sorodne podatke skupaj "zapakiramo" v strukture. 
 * Primer strukure za tocko v 2D celostevilski ravnini :
 */
struct imenik 
{
 int telefonskaStevilka;
 char ime[256];
 char priimek[256];
};
 struct tocka 
 {
	 int x;
	 int y;
 }; // ! Podpicje na koncu
// Funkcija, ki izpise tocko v ravnini
void izpisiTockoVRavnini( struct tocka t )
{
	printf("Tocka ( %d, %d )\n", t.x, t.y );
}
struct casVDnevu 
{
	int ura;
	int minuta;
	int sekunda;
};
void izpisiCasVDnevu(struct casVDnevu c )
{
	printf("%d:%d:%d\n", c.ura, c.minuta, c.sekunda );
}
/* Strukture : 
 * # Cas v letu (dan, mesec, leto)
 * # Cas v dnevu : ura, minuta, sekunda
 * # zmisli si 2
 * # funkcija ki izpise strukturo ( tocko v 3D, vnos v imenik, cas v dnevu, cas v letu )
 * # Funkcijo ki uporabnika vprasa za koordinate tocke v 3D in vrne to strukturo */
// Funkcija, ki vrne tocko v 2D 
struct tocka narediTocko2D( int x, int y )
{
	struct tocka t;
	t.x = x;
	t.y = y;
	return t;
}
int main(int argc, char **argv)
{
	/* Strukturo tocka uporabimo kot spremenljivko. 
	 * Najprej jo deklariramo : 
	 */
	// int x;
	struct tocka mojaTocka;
	/* Pri dekleraciji smo morali uporabiti 2 besedi : "struct" in "tocka" (ime strukture).
	 * Struktura sama se obnasa kot spremenljivka. Lahko njeno vrednost zapisemo v drugo strukturo enakega tipa.
	 * Spremenljivki x in y pa sta clana te strukture. 
	 * Do njuju lahko dostopamo z operatorjem "." :
	 */
	mojaTocka.x = 10;
	mojaTocka.y = 11;
	printf("Moja tocka je ( %d, %d )\n", mojaTocka.x, mojaTocka.y );
	struct tocka drugaTocka;
	drugaTocka = mojaTocka; // Kopiramo mojoTocko v drugo tocko
	printf("Druga tocka je ( %d, %d )\n", drugaTocka.x, drugaTocka.y );
	struct tocka tretjaTocka = narediTocko2D( 20, 30 );
	printf("Tretja tocka je ( %d, %d )\n", tretjaTocka.x, tretjaTocka.y );
	izpisiTockoVRavnini(mojaTocka);
	izpisiTockoVRavnini(drugaTocka);
	izpisiTockoVRavnini(tretjaTocka);
	
	struct casVDnevu c;
	c.ura = 12;
	c.minuta = 30;
	c.sekunda = 59;
	izpisiCasVDnevu(c);
	return 0;
}
