#include <stdio.h>

/*
 * Ponavljanje : 
 * - kazalci 
 * - datoteke 
 */

/*
 * Datoteke 
 * 
 * printf( ... ) -> fprintf( f, ... )
 * scanf( ... ) -> fscanf( f, ... )
 * FILE * f = fopen( "imeDatoteke.dat", "rt" );
 * r - beri 
 * w - pisi 
 * a - dodaj 
 * 
 * t - tekstovna datoteka
 * b - binarna datoteka
 * 
 * + - (odpremo za pisanje in branje)
 * 
 * fclose(f);
 */

int main(int argc, char **argv)
{
	FILE * f = fopen("pozdravljenaDatoteka.txt", "wt" );
	if ( f == NULL ) 
	{
		printf("Napaka pri odpiranju datoteke !\n" );
		return 0;
	}
	fprintf( f, "Pozdravljena datoteka !\n" );
	fclose(f);
	
	// Preberemo se eno datoteko 
	FILE * fBeri = fopen("beriDatoteko.txt", "rt" );
	if( fBeri == NULL )
	{
		printf("Napaka pri odpiranju datoteke za branje\n");
		return 0;
	}
	char vrstica[256];
	while( fscanf( fBeri, "%s", vrstica ) != EOF )
	{
		printf("Prebrali smo : %s\n", vrstica );
	}

	return 0;
}
