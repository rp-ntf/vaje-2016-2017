#include <stdio.h>
#include <math.h>

#include <iostream>

struct sVektor 
{
	float x;
	float y;
	float z;
};

class cVektor 
{
public :
	cVektor() {
		printf("Konstruktor razreda cVektor :");
		x = 0; y = 0; z = 0;
		izpisiVrednosti();
	}
	cVektor( float a, float b, float c ) {
		printf("Konstruktor razreda cVektor :");
		x = a; y = b; z = c;
		izpisiVrednosti();
	}
	~cVektor() {
		printf("Destruktor razreda cVektor : ");
		izpisiVrednosti();
	}
	float dolzina() {
		float d = sqrt( x*x + y*y + z*z );
		printf("Dolzina vektorja je %f\n", d );
		return d;
	}
	void nastavi( float a, float b, float c ) {
		x = a; y = b; z = c;
		printf("Vrednosti nastavljene : ");
		izpisiVrednosti();
	}
	void izpisiVrednosti() {
		std::cout << "( " << x << ", " << y << ", " << z << " )" << std::endl;
	}
	void pristejVektor( cVektor v ) {
		x = x + v.x; y = y + v.y; z = z + v.z;
	}
private :
	float x;
	float y;
	float z;
};

int main(int argc, char **argv)
{
	struct sVektor sv1;
	sv1.x = 0; sv1.y = 1; sv1.z = 2;
	printf("Zacenjamo z vektorskim razredom : \n");
	cVektor cv1(1,2,3), cv2(22,33,44);
	cv1.izpisiVrednosti();
	cv2.izpisiVrednosti();
	cv1.pristejVektor( cv2 );
	cv1.izpisiVrednosti();
	
	cVektor * pv; 
	pv = new cVektor[3];
	pv[1].izpisiVrednosti();
	delete [] pv;
	std::cout << "Koncujemo main" << std::endl;
	return 0;
}
