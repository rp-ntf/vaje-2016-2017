Vaje 
====

Na prvih vajah smo naredili program "pozdravljen svet", 
in se naučili nekaj osnov o spremenljivkah in izpisovanju na zaslon. 

Koda se med vajami večkrat spremeni, 
za to vam priporočamo, da izdelujete lastne zapiske. 

Tu je objavljena zadnja sprememba kode. 


Domača naloga 
-------------

1. sestavi program, ki prebere 3 mestno število (763), 
in ga izpiše v obratnem vrstnem redu (367)

2. sestavi program, ki prebere 2 kota v stopinjah, minutah in sekundah. 
Vsak kot ima 3 spremenljivke. 
Kota naj sešteje, in izpiše rezultat v 
stopinjah, minutah in sekundah. 

3. Vaja : program, ki pretvori med jardi in metri. (to smo naredili že na vajah)



