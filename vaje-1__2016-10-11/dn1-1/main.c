#include <stdio.h>

int main(int argc, char **argv)
{
	/* Navodilo : obrni 3 mesntno število */
	int n;
	printf("Vpisite 3 mestno stevilo : ");
	scanf( "%d", &n );
	printf("Vpisali ste : %d\n", n );
	int enice, desetice, stotice ;
	stotice = n / 100;
	desetice = (n-stotice*100)/10;
	enice = n-(stotice*100)-desetice*10;
	printf("Resitev 1 : Obrnjeno stevilo je : %d%d%d \n", enice, desetice, stotice );
	int obrnjeno;
	obrnjeno = 100*enice + 10*desetice + stotice;
	printf("Resitev 2 : Obrnjeno stevilo je : %d \n", obrnjeno );
	
	/* Z uporabo operacije modulo (%) */
	enice = (n%10) / 1;
	desetice = (n%100) / 10;
	stotice = (n%1000) / 100;
	printf("Resitev [modulo] : Obrnjeno stevilo je : %d%d%d \n", enice, desetice, stotice );
	
	return 0;
}
