// Program pretvori dolžino v jardih
// v dolžino v metrih, decimetrih in centimetrih

#include <stdio.h>

int main(void)
{
	float yard = 0.9144;
	float dolzinaYard;
	float dolzina;
	int m, dm, cm;
	
	printf("Vpisi dolzino v yardih: ");
	scanf("%f",&dolzinaYard);
	
	dolzina = dolzinaYard * yard;
	m = (int)dolzina;
	dolzina = 10 * (dolzina - m);
	dm = (int)dolzina;
	dolzina = 10 * (dolzina - dm);
	cm = (int)dolzina;
	
	printf("%g yardov je %d m, %d dm in %d cm.\n",dolzinaYard, m, dm, cm);
	return 0;
}
