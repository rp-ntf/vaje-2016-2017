#include <stdio.h>

int main(int argc, char **argv)
{
	/* Sestavite program, 
	 * ki prebere dve stevili, 
	 * in vrne absolutno vrednost njune razlike
	 */
	float x, y, rezultat;
	printf("Vnesite dve stevili : \n");
	scanf("%f %f", &x, &y);
	if( x > y )
		rezultat = x - y;
	else 
		rezultat = y - x;
	printf("| %f - %f | = %f\n", x, y, rezultat );
	return 0;
}
