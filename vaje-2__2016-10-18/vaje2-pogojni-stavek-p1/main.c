#include <stdio.h>

int main()
{
	/* imamo pravokotnik s koordinatami ogljišč (x1,y1) levo spodaj 
	 * ter (x2,y2) desno zgoraj in točko T (x,y). Kje leži točka T 
	 */
	int x1 = -1, y1 = 2, x2 = 3, y2 = 5;
	int x,y;
	
	printf("Vpisi koordinate tocke T(x,y)\n");
	scanf("%d %d",&x,&y);
	
	printf("Tocka T(%d,%d) lezi ",x,y);
	if (x>-1 && x<3 && y<5 && y>2) printf("v pravokotniku.\n");
	else {
		if (x<-1 || x>3 || y<2 || y>5) printf("zunaj pravokotnika.\n");
		else printf("na robu danega pravokotnika.\n");
	}
	
	return 0;
}
