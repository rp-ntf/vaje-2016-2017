#include <stdio.h>

int main(int argc, char **argv)
{
	/*
	 * Pogojni stavek : 
	 * 
	 * if( pogoj ) stavek ;
	 * 
	 * pogoj : logični izraz 
	 * stavek : stavek, ki se izvrši samo, če je pogoj izpolnjen
	 * stavek je lahko tudi t.i. "sestavljeni stavek" ali "blok stavek" : 
	 * 
	 * if ( pogoj )
	 * {
	 * 		stavek1;
	 * 		stavek2;
	 * 		stavek3;
	 * }
	 */
	// Absolutna vrednost stevila : 
	float x;
	printf("Vpisi realno stevilo: ");
	scanf("%f",&x);
	
	if (x<0) 
		x = -x;
	
	printf("absolutna vrednost vpisanega stevila je %g\n",x);
	
	/*
	 * Se ena oblika gnezdenja pogojnih stavkov : 
	 * if( pogoj1 ) stavek1 ;
	 * else if( pogoj2 ) stavek2;
	 * else if( pogoj3 ) stavek3;
	 * ....
	 * else if( pogojN ) stavekN;
	 * else stavekNplus1;
	 */
	
	return 0;
}
