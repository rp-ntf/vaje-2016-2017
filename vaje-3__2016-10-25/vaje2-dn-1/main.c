#include <stdio.h>
#include <math.h>

int main(int argc, char **argv)
{
	/*
	 * Domace naloge : 
	 * 1. Sestavi program, ki bo prebral sredisci in polmera dveh krogov, 
	 * ter preveril, ali sta kroga locena, se dotikata ali pa se sekata. 
	 */
	float x1, y1, r1, x2, y2, r2;
	printf("Vnesi sredisci in polmer dveh krogov v ravnini : \n");
	printf("Krog 1 : ");
	scanf("%f %f %f", &x1, &y1, &r1 );
	printf("Krog 2 : ");
	scanf("%f %f %f", &x2, &y2, &r2 );
	float dx = x1-x2, dy = y1-y2;
	float d = sqrt( dx*dx + dy*dy );
	float dr;
	if ( r1 > r2 ) 
		dr = r1-r2;
	else 
		dr = r2-r1;
	if( d > (r1+r2) || d < dr )
		printf("Kroznici sta loceni\n");
	else if( d == (r1+r2) || d == dr )
		printf("Kroznici se dotikata\n");
	else 
		printf("Kroznici se sekata\n");
	return 0;
}
