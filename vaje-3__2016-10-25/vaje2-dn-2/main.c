#include <stdio.h>
#include <math.h>

int main(int argc, char **argv)
{
	/*
	 * Domace naloge : 
	 * 2. Sestavi program, ki bo prebral 3 realna stevila, 
	 * in preveril, ali obstaja trikotnik s taksnimi dolzinami stranic. 
	 * V primeru da obstaja, naj program izracuna njegovo ploscino in obseg.
	 * Namig : heronov obrazec
	 */
	float a, b, c;
	printf( "Vnesite tri realna stevila : \n" );
	scanf( "%f %f %f", &a, &b, &c );
	// Pogoj da obstaja trikotnik : stevila ustrezajo trikotniski neenakosti 
	if( ( a < b+c ) && ( b < a+c ) && ( c < a+b ) )
	{
		float O, s, S;
		O = a+b+c;
		// Heronov obrazec https://en.wikipedia.org/wiki/Heron's_formula
		s = (a+b+c)/2;
		S = sqrt( s * (s-a) * (s-b) * (s-c) );
		printf("Stevila so lahko stranice trikotnika.\n");
		printf("Ploscina trikotnika je %f, in obseg %f.\n", S, O );
	}
	else 
	{
		printf("Stevila ne morejo biti stranice trikotnika\n");
	}
	return 0;
}
