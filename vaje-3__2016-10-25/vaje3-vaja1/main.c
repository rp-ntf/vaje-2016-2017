#include <stdio.h>

int main(int argc, char **argv)
{
	/*
	 * Naredi program, ki iz tipkovnice prebere 4 stevila, 
	 * in vrne najvecje in najmanjse stevilo
	 */
	float minx, maxx, a, b, c, d;
	printf("Vnesite 4 stevila : \n");
	scanf("%f %f %f %f", &a, &b, &c, &d );
	
	minx = a;
	maxx = a;
	if( b > maxx )
		maxx = b;
	if( b < minx )
		minx = b;
	if( c > maxx )
		maxx = c;
	if( c < minx )
		minx = c;
	if( d > maxx )
		maxx = d;
	if( d < minx )
		minx = d;
	// enako za c, d
	printf("Najvecje stevilo je : %f\n", maxx );
	printf("Najmanjse stevilo je : %f\n", minx );
	return 0;
}
