#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Program naj prebere rezultate 3 kolokvijev, 
	 * in izpiše, ali ste izpit naredili s kolokviji. 
	 * Izpit naredite s kolokviji, če : 
	 * - vsi kolokviji so pozitivni (>50%)
	 * - dva pozitivna in povprečje vseh treh kolokvijev je > 50%
	 */
	printf("Vnesite rezultat na 3 kolokvijih v procentih\n");
	float a,b,c;
	scanf("%f %f %f", &a, &b, &c );
	if( a>50 && b>50 && c>50 )
		printf("Opravili ste izpit\n");
	else
	{
		float povprecje = (a+b+c)/3;
		if( povprecje <= 50 )
			printf("Izpita niste opravili\n");
		else if( (a>50 && b>50) 
			|| (a>50 && c>50) 
			|| (b>50 && c>50) 
			)
			printf("Opravili ste izpit\n");
		else 
			printf("Izpita niste opravili\n");
	}
	return 0;
}
