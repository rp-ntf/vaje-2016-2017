#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * imate funkcijo 
	 * f(x) = odsekoma podana :
	 *      x < 0 : f(x) = x+2
	 * 0 <= x < 3 : f(x) = x * x - 3
	 * 3 <= x < 7 : f(x) = (x+1) * (x+1) 
	 * 7 <= x     : f(x) = 3 * x
	 */
	float f, x;
	printf("Vnesite x : ");
	scanf("%f", &x );
	if( x < 0 )
		f = x + 2;
	else if( 0 <= x && x < 3 )
		f = x * x - 3;
	else if( 3 <= x && x < 7 )
		f = (x+1) * (x+1);
	else if( 7 <= x )
		f = 3 * x;
	printf( "f( %f ) = %f\n", x, f );
	return 0;
}
