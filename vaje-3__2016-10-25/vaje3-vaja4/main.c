#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Uporabnik naj vpise dve polji 
	 * s koordinatami na sahovnici 
	 * (x, y), 1<= x,y <=8
	 * 
	 * 1. Program naj izpise, ali sta polji enake 
	 * ali razlicne barve.
	 * 
	 * Program naj izpise ali figura X na prvem 
	 * polju napada (poje) drugo polje : 
	 * 2. figura je trdnjava 
	 * 3. figura je lovec
	 * 4. figura je konj (skakac)
	 * 5. figura je kraljica 
	 * 6. figura je kralj
	 * 
	 * Domaca naloga : 
	 * Definirajte IN resite nalogo, 
	 * ki bo uporabljala vsaj 3 if stavke.
	 */
	int x1, x2, y1, y2;
	printf("Vpisite koordinato polja 1 : ");
	scanf("%d %d", &x1, &y1 );
	printf("Vpisite koordinato polja 2 : ");
	scanf("%d %d", &x2, &y2 );
	
	if( ((x1+y1) % 2) == ((x2+y2) % 2) )
		printf("Polji sta enake barve\n");
	else 
		printf("Polji nista enake barve\n");
	
	if( x1==x2 || y1==y2 )
		printf("Trdnjava napada polje\n");
	else 
		printf("Trdnjava ne napada polja\n");
	
	int dx = x1-x2, dy = y1-y2;
	if( dx == dy || dx == -dy )
		printf("Lovec napada polje\n");
	else 
		printf("Lovec ne napada polja\n");
	
	if( 
		( ( (dx==2 || dx==-2) && (dy==1 ||dy==-1) ) )
		|| ( (dx==1 || dx==-1) && (dy==2 || dy==-2) )
		)
		printf("Konj napada polje\n");
	else 
		printf("Konj ne napada polja\n");
	
	if( ( x1==x2 || y1==y2 ) 
		|| ( dx == dy || dx == -dy ) )
		printf("Kraljica napada polje\n");
	else 
		printf("Kraljica ne napada polja\n");
	
	if( (-1<=dx && dx<=1) 
		&& (-1<=dy && dy <=1) )
		printf("Kralj napada polje\n");
	else 
		printf("Kralj ne napada polja\n");
	
	return 0;
}
