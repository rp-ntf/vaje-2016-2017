#include <stdio.h>

int main(int argc, char **argv)
{
	/* pogojni stavek : 
	 * if( pogoj ) stavek;
	 * zanka while : 
	 * while( pogoj ) stavek;
	 * Stavek lahko vedno zamenjamo z blok stavkom : 
	 * while( pogoj ) 
	 * {
	 * 		stavek1;
	 * 		stavek2;
	 * 		...
	 * }
	 */
	int x = 0;
	while( x < 10 )
	{
		if( x % 2 == 1 )
			printf("%d\n", x );
		x = x + 1;
	}
	/* Domača naloga : 
	 * napisite program, 
	 * ki "na roko" zdeli dve stevili. 
	 * Programu podate dve stevili in stevilo decimalk. 
	 * Program izpise rezultat na izbrano stevilo decimalk.
	 */
	return 0;
}
