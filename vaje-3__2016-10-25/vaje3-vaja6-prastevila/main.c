#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Program naj uporabnika vprasa za naravno stevilo, 
	 * in izpise, ali je vpisano stevilo prastevilo
	 */
	int i,j;
	printf("Vnesite naravno stevilo : ");
	scanf("%d", &i );
	j = 2;
	while( j < i )
	{
		printf(">> %d %% %d = %d\n", i, j, i%j );
		if( i % j == 0 )
		{
			j = i+1;
			printf("Stevilo ni prastevilo\n");
		}
		j = j + 1;
	}
	if( j==i )
		printf("Stevilo je prastevilo\n");
	return 0;
}
