#include <stdio.h>

int main()
{
	/* Domača naloga : 
	 * napisite program, 
	 * ki "na roko" zdeli dve stevili. 
	 * Programu podate dve stevili in stevilo decimalk. 
	 * Program izpise rezultat na izbrano stevilo decimalk.
	 */
	
	/* Najprej preberemo podatke od uporabnika : */
	int a, b, n;
	printf( "Povejte stevilo a : " );
	scanf( "%d", &a );
	printf( "Povejte stevilo b : " );
	scanf( "%d", &b );
	printf( "Povejte stevilo decimalk : " );
	scanf( "%d", &n );
	/* Celi del pri deljenju : */
	int celi_del = a / b;
	int ostanek = a - b * celi_del ;
	printf("Delimo %d / %d na %d decimalk\n", a, b, n );
	printf( "%d / %d = %d.", a, b, celi_del );
	
	int i_decimalka = 1, ta_decimalka;
	while( i_decimalka < n )
	{
		// Ostanek mnozimo z 10, ker smo v desetiskem sistemu 
		ostanek = ostanek * 10;
		// Naslednja decimalka : delimo ostanek z deljiteljem
		ta_decimalka = ostanek / b;
		ostanek = ostanek - b * ta_decimalka;
		// Lahko tudi : 
		// ostanek = ostanek % b;
		printf("%d", ta_decimalka );
		i_decimalka += 1;
	}
	printf("\n");
	
	return 0;
}
