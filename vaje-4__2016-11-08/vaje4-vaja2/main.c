#include <stdio.h>

int main(int argc, char **argv)
{
	/* Sestavi program, 
	 * kjer uporabnik vpisuje (cela) stevila
	 * dokler uporabnik ne vnese stevila 0. 
	 * Ko uporabnik vnese stevilo 0, se program zakljuci
	 * in izpise povprecno vrednost vseh LIHIH stevil.
	 */
	int i = 0; // stevec za liha stevila 
	int i_vsa_stevila = -1; // stevec za soda stevila
	int x = 1; // Zadnje prebrano stevilo
	int vsota = 0; // Vsota vseh stevil do zdaj

	while( x != 0 )
	{
		printf( "Vnesite novo stevilo : " );
		scanf("%d", &x );
		if( x % 2 == 1 )
		{
			i = i + 1;
			vsota = vsota + x;
		}
		i_vsa_stevila = i_vsa_stevila + 1;
	}
	printf("Vsota = %d\n", vsota );
	printf( "Vnesli smo %d stevil, povprecje je %f\n", 
			i_vsa_stevila, ((float)vsota)/i );
	return 0;
}
