#include <stdio.h>

int main(int argc, char **argv)
{
	/* Sestavi program, 
	 * kjer uporabnik vpisuje (pozitivna, realna) stevila
	 * dokler uporabnik ne vnese stevila 0. 
	 * Ko uporabnik vnese stevilo 0, se program zakljuci
	 * in izpise povprecno vrednost vseh stevil.
	 */
	float i = 0; // stevec
	float x = 1.0; // Zadnje prebrano stevilo
	float vsota = 0.0; // Vsota vseh stevil do zdaj
	while( x != 0 )
	{
		printf( "Vnesite novo stevilo : " );
		scanf("%f", &x );
		i = i + 1;
		vsota = vsota + x;
	}
	i = i - 1;
	printf( "Vnesli smo %g stevil, povprecje je %g\n", i, vsota/i );
	
	return 0;
}
