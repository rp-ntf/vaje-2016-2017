#include <stdio.h>

int main(int argc, char **argv)
{
	/* do { stavek1; stavek2; ... } while(pogoj); */
	/*
	 * do {
	 * stavek1;
	 * stavek2;
	 * ...
	 * } while ( pogoj );
	 */
	/* Naloga : 
	 * Uporabnik naj vpise celo stevilo, 
	 * program pa naj izpise stevilo stevk v tem stevilu. 
	 * Npr : 
	 * 7352 -> 4
	 */
	int st_stevk = 0;
	int n, n_vpisan;
	printf("Vpisite stevilo : ");
	scanf("%d", &n );
	n_vpisan = n;
	do {
		n = n / 10;
		st_stevk = st_stevk + 1;
	} while( n != 0 );
	printf("[do ... while] Stevilo stevk v stevilu %d je %d\n", 
		n_vpisan, st_stevk );
	/* do stavek while( pogoj );
	 * je enako kot 
	 * stavek ; while( pogoj ) stavek;
	 */
	st_stevk = 0;
	n = n_vpisan;
	{
		n = n / 10;
		st_stevk = st_stevk + 1;
	}
	while( n != 0 ) {
		n = n / 10;
		st_stevk = st_stevk + 1;
	}
	printf("[while] Stevilo stevk v stevilu %d je %d\n", 
		n_vpisan, st_stevk );
	return 0;
}
