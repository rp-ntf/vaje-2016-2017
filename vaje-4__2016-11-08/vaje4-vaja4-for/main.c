#include <stdio.h>

int main(int argc, char **argv)
{
	/* Zanka for */
	/* Primer z "while" : 
	 * int i=0;
	 * while ( 
	 */
	int i;
	printf("Primer z while zanko :\n");
	i = 0;
	while( i < 5 )
	{
		printf("%d\n",i);
		i = i + 1;
	}
	printf("Primer z for zanko : \n");
	/* for( izraz1; pogoj; izraz2 ) stavek ; */
	for( i=0; i<5; ++i )
		printf("%d\n",i);
	// i = i+1 
	// se lahko zapiše tudi kot 
	// i++
	// ali pa kot 
	// ++i 
	return 0;
}
