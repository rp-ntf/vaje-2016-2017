#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Sestavi program, ki od uporabnika prebere stevilo n, 
	 * in izpise vse lihe kvadrate do n. 
	 * Uporabi for zanko.
	 * 
	 * Domaca naloga :
	 * Sestavi program, ki od uporabnika prebere stevilo n,
	 * (1. tudi stevilo m),
	 * program pa naj na zaslon izpise (primer za n=3, m=2) : 
	 * 1. Pravokotnik s stranicami n x m
	 * ***
	 * ***
	 * 2. Enakokrak trikotnik visine n
	 *   *  
	 *  *** 
	 * *****
	 */
	int n;
	printf("Vnesi stevilo n : \n");
	scanf("%d", &n );
	int i;
	for( i=0; i*i<n; i++ )
	{
		if( (i*i)%2 == 1 )
			printf("%d ", i*i );
	}
	printf("\n");
	return 0;
}
