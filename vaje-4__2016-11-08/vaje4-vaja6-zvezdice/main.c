#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Od uporabnika preberi stevilo n
	 * in izpisi pravokotni trikotnik z visino n. 
	 * Za n = 4 naj program izpise : 
	 * *
	 * **
	 * ***
	 * ****
	 */
	int i;
	printf("4 zvezdice vodoravno : \n");
	for( i=0; i<4; ++i )
		printf("*");
	printf("\n");
	printf("4 zvezdice navpicno : \n");
	for( i=0; i<4; ++i )
		printf("*\n");
	printf("\n");
	int n = 5;
	printf("Vpisite stevilo n : ");
	scanf("%d", &n);
	printf("Pravokotni trikotnik s stranico %d : \n", n );
	for( i=0; i<n; ++i )
	{
		int j;
		for( j=0; j<=i; ++j )
			printf("*");
		printf("\n");
	}
	return 0;
}
