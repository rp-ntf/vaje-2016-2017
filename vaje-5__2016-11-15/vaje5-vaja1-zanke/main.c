#include <stdio.h>

int main()
{
	/* Naloga : 
	 * Sestavi program, ki bere iz tipkovnice vpisana cela stevila, 
	 * dokler ne prebere stevila 0,
	 * in izracuna njihovo povprecno vrednost, in povprecno absolutno vrednost stevilo
	 * Dodatna naloga : 
	 * od uporabnika preberite vsaj 10 stevil. Ce pred tem vnese 0, ga opozrite in nadaljujte z branjem stevil
	 */
	int x, abs_x, vsota = 0, vsota_abs = 0, stevec = 0;
	float povprecje, povprecje_abs;
	do {
			printf("Vnesite stevilo : ");
			scanf("%d",&x);
			if( x == 0 && stevec < 10 )
			{
				printf("Potrebujemo vsaj 10 stevil, do zdaj ste vnesli %d stevil.\n", stevec );
				x = 1;
			}
			else
			{
				if( x >= 0 )
					abs_x = x;
				else 
					abs_x = -x;
				vsota = vsota + x;
				vsota_abs = vsota_abs + abs_x;
				stevec = stevec + 1;
			}
	} while( x != 0 );
	stevec = stevec - 1; // da ne stejemo 0
	povprecje = ((float)vsota) / stevec;
	povprecje_abs = ((float)vsota_abs) / stevec;
	printf("\n");
	printf("Vnesli ste %d stevil.\n", stevec );
	printf("Povprecna vrednost stevil je %f.\n", povprecje );
	printf("Povprecna vrednost absolutnih vrednosti stevil je %f.\n", povprecje_abs );
	return 0;
}
