#include <stdio.h>

int main()
{
	/* Naloga : 
	 * kot prej.
	 * 
	 * S stavkom continue : prilagodi resitev, tako da bo uporabljala stavek continue.
	 * Ce se v zanki izvrsi stavek "continue", se zanka ponovno izvrsi.
	 * Primer : 
	 * while( x < 10 )
	 * {
	 * 		printf("%d\n", x );
	 * }
	 */
	int x, abs_x, vsota = 0, vsota_abs = 0, stevec = 0;
	float povprecje, povprecje_abs;
	do {
			printf("Vnesite stevilo : ");
			scanf("%d",&x);
			if( x == 0 && stevec < 10 )
			{
				printf("Potrebujemo vsaj 10 stevil, do zdaj ste vnesli %d stevil.\n", stevec );
				x = 1;
				continue;
			}
			if( x >= 0 )
				abs_x = x;
			else 
				abs_x = -x;
			vsota = vsota + x;
			vsota_abs = vsota_abs + abs_x;
			stevec = stevec + 1;
	} while( x != 0 );
	stevec = stevec - 1; // da ne stejemo 0
	povprecje = ((float)vsota) / stevec;
	povprecje_abs = ((float)vsota_abs) / stevec;
	printf("\n");
	printf("Vnesli ste %d stevil.\n", stevec );
	printf("Povprecna vrednost stevil je %f.\n", povprecje );
	printf("Povprecna vrednost absolutnih vrednosti stevil je %f.\n", povprecje_abs );
	return 0;
}
