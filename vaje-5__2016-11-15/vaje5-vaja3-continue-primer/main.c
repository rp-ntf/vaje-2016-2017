#include <stdio.h>

int main(int argc, char **argv)
{
	/* Primer uporabe stavka continue / break
	 */
	int x = 0;
	while( x < 10 ) 
	{
		printf("Zacnemo zanko\n");
		x = x + 1;
		if( x % 3 == 0 )
			continue;
		if ( x == 7 ) // Vaja : preizkusi pogoje x == 5, x == 6 in x == 7
			break;
		printf("x = %d\n", x );
		printf("Koncamo zanko\n");
	}
	return 0;
}
