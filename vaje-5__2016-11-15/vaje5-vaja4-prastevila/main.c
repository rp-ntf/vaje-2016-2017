#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga 1 : 
	 * od uporabnika preberite stevilo, manjse od 100. 
	 * Ce je stevilo preveliko, uporabnika opozorite naj vnese drugo stevilo.
	 * Sicer pa izpisite vsa prastevila, do vkljucno izbranega stevila. 
	 * Program naj takoj, ko najde prvi deljitelj nekega stevila, nadaljuje s testiranjem naslednjega stevila.
	 */
	int n;
	// Preberemo stevilo n :
	do {
		printf("Vnesite stevilo <= 100 : ");
		scanf("%d", &n );
	} while ( n > 100 );
	int i=1;
	for( i = 1; i <= n ; i = i+1 )
	{
		// Preverimo, ali je stevilo i prastevilo 
		int j;
		for( j=2; j<i; j = j + 1 )
		{
			if( i % j == 0 ) // Nasli smo deljitelj stevila i, torej i ni prastevilo
				break;
		}
		if( i == j )
			printf("%d ", i );
	}
	printf("\n");
	return 0;
}
