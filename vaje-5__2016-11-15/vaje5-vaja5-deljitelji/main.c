#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga 2 : 
	 * Sestavi program, ki prebere naravno stevilo n in izpise vse njegove deljitelje, in skupno stevilo deljiteljev.
	 */
	int n;
	int st_deljiteljev = 0;
	int deljitelj;
	printf("Vnesite naravno stevilo : ");
	scanf("%d", &n );
	for( deljitelj = 1; deljitelj <= n; deljitelj++ )
	{
		if( n % deljitelj == 0 )
		{
			printf("%d ", deljitelj );
			st_deljiteljev++;
		}
	}
	printf("\n");
	printf("Stevilo %d ima %d deljiteljev.\n", n, st_deljiteljev );
	return 0;
}
