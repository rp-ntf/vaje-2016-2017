#include <stdio.h>
#include <math.h>
int main(int argc, char **argv)
{
	/* Domača naloga : 
	 * exp(x) = 1 + x^1 / (1!) + x^2 / (2!) + x^3/(3!) + ... + x^n / (n!) 
	 * Izpisite vrednosti zgornje vrste za n = 1, ..., 20 
	 * in razliko vsote zgornje vrste z pravo vrednostjo za vsak n.
	 */
	float x;
	printf("Vnesite realno stevilo : ");
	scanf("%f", &x );
	printf("exp( %f ) = %f \n", x, exp(x) );
	return 0;
}
