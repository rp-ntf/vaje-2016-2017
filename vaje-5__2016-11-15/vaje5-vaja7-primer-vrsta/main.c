#include <stdio.h>
#include <math.h>
int main(int argc, char **argv)
{
	/* Domača naloga : 
	 * vrsta = 1 + 5/6^2 + ... + 5^n/6^(2n)
	 * Izpisite vrednosti zgornje vrste za n = 1, ..., 20 
	 * in razliko vsote zgornje vrste z pravo vrednostjo za vsak n.
	 * Vrednost clenov sproti izpisujmo.
	 */
	float clen, vsota=0;
	int i, n=20;
	clen = 1.0;
	vsota = clen;
	for( i = 1; i <= n; ++i )
	{
		clen = clen * 5.0 / (6.0 * 6.0);
		vsota = vsota + clen;
		printf("Clen %d = %f, vsota = %f\n", i, clen, vsota );
	}
	return 0;
}
