#include <stdio.h>



int main(int argc, char **argv)
{
	/* Domaca naloga : 
	 * Sestavite program, ki pretvarja med rimskimi stevili 
	 * in desetiskimi stevili.
	 * Program naj uporabnika vprasa za stevilo, 
	 * in na zaslon izpise to stevilo, predstavljeno 
	 * z rmiskimi stevili.
	 * Ko uporabnik vpise 0, naj program preneha.
	 * Osnovni program naj pretvarja stevila do vkljucno 21.
	 * 
	 * Bonus naloga : 
	 * - program razsirite z moznostmi pretvarjanja 
	 * 	rimskih stevil do 1000
	 * - programu dodajte moznost pretvarjanja v obratni smeri,
	 * 	ob vnesenem rimskem stevilu naj program izpise
	 * 	njegovo vrednost v desetiskem sestavu.
	 */
	printf("hello world\n");
	return 0;
}
