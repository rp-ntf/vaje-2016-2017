#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Program naj uporabnika vprasa za naravno stevilo n, 
	 * in izpise enakokrak trikotnik visine n.
	 * n = 3 :
	 *   *
	 *  ***
	 * *****
	 */
	printf("Vpisite naravno stevilo n : ");
	int n;
	scanf("%d",&n);
	
	int i,j;
	for( i=0; i<n; ++i )
	{
		for( j=0; j<n-i; ++j )
			printf(" ");
		for( j=0; j<2*i+1; ++j )
			printf("*");
		printf("\n");
	}
	return 0;
}
