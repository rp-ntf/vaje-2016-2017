#include <stdio.h>
#include <math.h>
int main(int argc, char **argv)
{
	/* Domača naloga : 
	 * exp(x) = 1 + x^1 / (1!) + x^2 / (2!) + x^3/(3!) + ... + x^n / (n!) 
	 * Izpisite vrednosti zgornje vrste za n = 1, ..., 20 
	 * in razliko vsote zgornje vrste z pravo vrednostjo za vsak n.
	 */
	float x;
	printf("Vnesite vrednost x = ");
	scanf("%f", &x );
	float clen, vsota=0;
	int i, n=20;
	clen = 1.0;
	vsota = clen;
	printf("i | clen |vsota | prava vrednost | razlika\n");
	for( i = 1; i <= n; ++i )
	{
		clen = clen * x / i;
		vsota = vsota + clen;
		printf("%d | %f |%f | %f | %f\n", 
		i, clen, vsota, exp(x), vsota-exp(x) );
	}
	return 0;
}
