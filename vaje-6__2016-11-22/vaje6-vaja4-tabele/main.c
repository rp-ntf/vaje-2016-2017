#include <stdio.h>

int main(int argc, char **argv)
{
	/* Tabele : 
	 * primerjava tabele 5 stevil in ene same spremenljivke :
	 * int n; // ena spremenljivka
	 * int tabela[5]; // 5 stevil
	 * int n=10; // definicija n
	 * int tabela[] = { 1, 2, 3, 4, 5 }; // definicija tabele
	 * int tabela[5] = { 1, 2, 3, 4, 5 }; // se ena definicija
	 */
	/* Vaja :
	 * Preberimo 5 stevil iz tipkovnice, 
	 * in jih izpisemo v obratnem vrstnem redu.
	 */
	int tab[5];
	int i;
	for( i=0; i<5; ++i )
	{
		printf("Vnesite %d-to stevilo : ",i);
		scanf("%d", &tab[i] );
	}
	printf("Izpisujemo stevila : \n");
	for( i=4; i>=0; --i )
	{
		printf("tabela [%d] = %d\n", i, tab[i] );
	}
	return 0;
}
