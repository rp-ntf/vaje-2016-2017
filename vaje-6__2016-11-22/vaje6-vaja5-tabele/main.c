#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Sestavite program, kjer uporabnik najprej 
	 * vpise podatke v tabelo.
	 * Vnese lahko najvec 10 stevil.
	 * Nato pa program sprasuje uporabnika po celih stevilih, 
	 * in izpise, na katerem mestu v tabeli se stevilo najprej pojavi, 
	 * ali pa izpise, da stevila ni v tabeli.
	 * Vnos stevil zakljucimo s stevilom 0.
	 */
	printf("Vpisite stevilo stevil v tabeli : ");
	int n;
	int i;
	int tab[10];
	scanf("%d",&n ); // Zapomnimo si stevilo stevil
	for( i=0; i<n; ++i )
	{
		printf("Vnesite stevilo %d : ", i );
		scanf("%d", &tab[i] );
	}
	// Zdaj pa uporabnika sprasujemo po celih stevilih.
	int stevilo;
	while( 1 )
	{
		printf("Vnesite stevilo za iskanje : ");
		scanf("%d", &stevilo );
		if( stevilo == 0 ) 
			break;
		for( i=0; i<n; ++i )
		{
			if( tab[i] == stevilo )
			{
				printf("Stevilo %d smo nasli na %d-tem mestu tabele, tab[%d] = %d.\n",
				stevilo, i, i, stevilo );
				break;
			}
		}
		if( i==n )
			printf("Stevila %d nismo nasli v tabeli.\n", stevilo );
	}
	return 0;
}
