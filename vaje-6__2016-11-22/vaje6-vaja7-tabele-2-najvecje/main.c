#include <stdio.h>

int main(int argc, char **argv)
{
	/* Sestavi program, ki prebere stevila, ki jih vpise uporabnik.
	 * Najvec 50 stevil, bere dokler ne vpisete 0.
	 * Program nakoncu izpise 2. najvecje stevilo
	 * izmed vpisanih stevil.
	 * dodatna naloga : 
	 * Ko uporabnik vpise vsa stevila, ga vprasate katero najvecje stevilo po vrsti 
	 * naj program izpise. Uporabnik vpise npr 3, 
	 * potem program izpise 3. najvecje stevilo v tabeli.
	 * 
	 * Domaca naloga : 
	 * program ugotovi se, ali je vpisana tabela stevil ne-padajoca.
	 * Primer ne-padajoce tabele : 
	 * 1, 2, 3, 3, 7, 7, 8,
	 */ 
	int tab[50];
	int i;
	int n = 50; // uporabljena velikost tabele
	for( i=0; i < 50; i++ )
	{
		printf("Vnesite stevilo %d : ", i );
		scanf("%d", &tab[i] );
		if( tab[i] == 0 ) 
		{
			n = i;
			break;
		}
	}
	// Zdaj pa najdemo 2. najvecje stevilo : 
	int n_najvecje = 2;
	int i_najvecje;
	int najvecje_stevilo;
	for( n_najvecje = 2; n_najvecje > 0; n_najvecje-- )
	{
		i_najvecje = 0;
		for( i=0; i<n; ++i )
		{
			if( tab[i] > tab[i_najvecje] )
				i_najvecje = i;			
		}
		najvecje_stevilo = tab[i_najvecje];
		tab[i_najvecje] = 0;
	}
	printf("%d-to najvecje stevilo je %d\n", 2, najvecje_stevilo );
	return 0;
}
