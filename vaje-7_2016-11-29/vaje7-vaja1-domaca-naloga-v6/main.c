#include <stdio.h>

int main(int argc, char **argv)
{
	/* Domaca naloga : 
	 * Sestavite program, ki pretvarja med rimskimi stevili 
	 * in desetiskimi stevili.
	 * Program naj uporabnika vprasa za stevilo, 
	 * in na zaslon izpise to stevilo, predstavljeno 
	 * z rmiskimi stevili.
	 * Ko uporabnik vpise 0, naj program preneha.
	 * Osnovni program naj pretvarja stevila do vkljucno 21.
	 * 
	 * Bonus naloga : 
	 * - program razsirite z moznostmi pretvarjanja 
	 * 	rimskih stevil do 1000
	 * - programu dodajte moznost pretvarjanja v obratni smeri,
	 * 	ob vnesenem rimskem stevilu naj program izpise
	 * 	njegovo vrednost v desetiskem sestavu.
	 */
	// Seznam vseh posebnih stevil :
	int rimska_stevila[5] = { 1, 4, 5, 9, 10 };
	printf("Vnesite desetisko stevilo : ");
	int stevilo;
	scanf("%d", &stevilo );
	int naslednje_stevilo;
	int i;
	printf("Rimsko stevilo je : ");
	fflush(stdout); // S tem prisilimo izpis vsega na zaslon, racunalnik z izpisom vcasih pocaka do interakcije z uporabnikom.
	while( stevilo > 0 )
	{
		for( i=4; i>=0; --i )
		{
			if( stevilo >= rimska_stevila[i] )
			{
				naslednje_stevilo = rimska_stevila[i];
				break;
			}
		}
		// Izpisemo naslednje stevilo : 
		if( naslednje_stevilo == 1 )
			printf("I");
		else if( naslednje_stevilo == 4 )
			printf("IV");
		else if( naslednje_stevilo == 5 )
			printf("V");
		else if( naslednje_stevilo == 9 )
			printf("IX");
		else if( naslednje_stevilo == 10 )
			printf("X");
		else 
			printf("NAPAKA");
		stevilo -= naslednje_stevilo;
		fflush(stdout); // S tem prisilimo izpis vsega na zaslon, racunalnik z izpisom vcasih pocaka do interakcije z uporabnikom.
	}
	printf("\n");
	return 0;
}
