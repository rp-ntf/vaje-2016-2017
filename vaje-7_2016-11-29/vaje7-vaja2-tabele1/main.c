#include <stdio.h>

int main(int argc, char **argv)
{
	/* Naloga : 
	 * Sestavite program, ki od uporabnika najprej prebere dolzino tabele (najvec 100), 
	 * potem pa od uporabnika prebere toliko celih stevil.
	 * Ko so stevila prebrana, program najprej izpise tabelo v eni vrstici. 
	 * Potem pa program najde en primer dveh stevil na zaporednih mestih v tabeli, 
	 * in ju uredi po velikosti. 
	 * Na koncu program izpise novo tabelo.
	 * Primer : 
	 * tabela dolzine 5 : 
	 * 1 2 4 3 2
	 * 1 2 3 4 2
	 * Bonus naloga : 
	 * naj program zamenjuje dve zaporedni stevili vse dokler tabela ni povsem urejena.
	 */
	/* Namig : dve zaporedni stevili v tabeli zamenjamo tako : 
	 * int zacasno;
	 * zacasno = tabela[i];
	 * tabela[i] = tabela[i+1];
	 * tabela[i+1] = zacasno;
	 * 
	 * 1 10 2 20 3
	 * 1 2 10 20 3
	 * 
	 */	 
	printf("Vnesite dolzino tabele : ");
	int dolzina_tabele; 
	scanf("%d", &dolzina_tabele );
	int tabela[100]; // Tabela maksimalne dolzine 100
	int i;
	for( i=0; i<dolzina_tabele; ++i )
	{
		printf("Vnesite tabela[%d] = ", i );
		scanf("%d", &tabela[i] );
	}
	for( i=0; i<dolzina_tabele; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	// Najdemo prvi primer, kjer je : tabela[i] > tabela[i+1]
	int zacasno;
	for( i=0; i<dolzina_tabele-1; ++i )
	{
		if( tabela[i] > tabela[i+1] )
		{
			// Zamenjamo vrednos
			zacasno = tabela[i];
			tabela[i] = tabela[i+1];
			tabela[i+1] = zacasno;
			// prekinemo zanko
			break;
		}
	}
	printf("Deloma urejena tabela : \n");
	for( i=0; i<dolzina_tabele; ++i )
	{
		printf("%d ", tabela[i] );
	}
	printf("\n");
	
	return 0;
}
