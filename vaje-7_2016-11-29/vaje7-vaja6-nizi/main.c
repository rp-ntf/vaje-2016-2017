#include <stdio.h>
#include <string.h>
int main(int argc, char **argv)
{
	/* Naloga : 
	 * Naredite program, ki od uporabnika v zanki 
	 * prebira niz, in izpise ta niz skupaj z njegovo dolzino. 
	 * Program se konca, ko uporabnik vpise niz "konec".
	 */
	char niz[256]; 
	char konec[] = "konec";
	while( strcmp(niz,konec) != 0 ) 
	{
		printf("Vnesite niz znakov : ");
		scanf("%s", niz );
		printf("niz %s,  dolg %d znakov\n", niz, strlen(niz) );
	}
	
	/* Naloga : 
	 * Naredite program, 
	 * ki od uporabnika prebere niz znakov, 
	 * a) in izpise, kolikokrat se v njem pojavi crka 'a'.
	 * b) in izpise, kolikokrat se v njem pojavi samoglasnik. 
	 * Primer : 
	 * To je testen stavek 
	 * stevilo samoglasnikov : 6
	 */
	 
	/* Domaca naloga : 
	 * Naredite program, ki od uporabnika prebere niz znakov (beseda).
	 * Program potem izpise niz znakov v obratnem vrstenm redu.
	 * Primer : abcdefg -> gfedcba
	 */
	return 0;
}
