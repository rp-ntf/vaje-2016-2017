#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	/* Domaca naloga : 
	 * Naredite program, ki od uporabnika prebere niz znakov (beseda).
	 * Program potem izpise niz znakov v obratnem vrstenm redu.
	 * Primer : abcdefg -> gfedcba
	 */
	char vrstica[256];
	printf("Vnesite niz znakov : ");
	scanf("%s", vrstica );
	printf("Niz znakov v obratnem vrstnem redu je : ");
	int i;
	for( i=strlen(vrstica)-1; i>=0; --i )
		printf("%c", vrstica[i] );
	printf("\n");
	return 0;
}
