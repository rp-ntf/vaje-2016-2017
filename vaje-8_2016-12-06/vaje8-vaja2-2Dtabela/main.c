#include <stdio.h>

int main(int argc, char **argv)
{
	/* Tabele v programskem jeziku C 
	 * lahko zajemajo vecdimenzionalne podatke.
	 * Na primer 2D tabela int dimenzije 3x5 : (3 vrstice, 5 stolpcev)
	 */
	int tabela3x5[3][5];
	// Ko jih kreiramo, lahko z gnezdenjem { { } } dolocimo vrednosti vsem elementom. 
	// Npr : 
	int tabela4[4] = { 1, 2, 3, 4 };
	int tabela2x3[2][3] = { { 1, 2, 3 }, { 4, 5, 6 } };
	// Do elementov 2D tabele dostopamo enako kot do elementov 1D tabel, z [] :
	int i, j;
	printf("Prvotna tabela : \n");
	for( i=0; i<2; ++i )
	{
		for( j=0; j<3; ++j )
		{
			printf("%d ", tabela2x3[i][j] );
		}
		printf("\n");
	}
	// Nastavimo elemente 2. vrstice na 100 + element 1. vrstice : :
	for( j=0; j<3; ++j )
	{
		tabela2x3[1][j] = 100 + tabela2x3[0][j];
	}
	printf("Popravljena tabela : \n");
	for( i=0; i<2; ++i )
	{
		for( j=0; j<3; ++j )
		{
			printf("%d ", tabela2x3[i][j] );
		}
		printf("\n");
	}
	
	return 0;
}
