#include <stdio.h>

/* Naloge (napisi funkcijo in jo uporabi v main funkciji) : 
 * Sestavite funkcijo, ki sesteje 3 cela stevila.
 * Sestavite funkcijo, ki vrne razliko 2 cela stevil.
 * Sestavite funkcijo, ki vrne kvocient dveh realnih stevil. 
 * Sestavite funkcijo, ki na zaslon izpise 
 * 3 znake, ki jih dobi kot argument.
*/
int sestejTri( int x, int y, int z )
{
	return x + y + z;
}
int razlikaDvehStevil( int x, int y )
{
	return x - y;
}
float kvocient( float x, float y )
{
	return x / y;
}
void izpisiTriZnake( char znak1, char znak2, char znak3 )
{
	printf("Trije znaki so : %c%c%c\n", znak1, znak2, znak3 );
}
int produktDvehVsot( int a, int b, int c, int d )
{
	return (a+b) * (c+d);
}
void aliJeDeljivoZDeset(int stevilo)
{
	if( stevilo % 10 == 0 )
		printf("Stevilo je deljivo z 10\n");
	else 
		printf("Stevilo ni deljivo z 10\n");
}
void skupnaDolzinaDvehNizov( char niz1[], char niz2[] )
{
	printf("Skupna dolzina nizov %s in %s je %d.\n",
		niz1, niz2, 
		strlen(niz1) + strlen(niz2) );
}
int main(int argc, char **argv)
{
	int vsota;
	vsota = sestejTri(1,2,3);
	printf("Vsota 1, 2 in 3 je : %d\n", vsota );
	izpisiTriZnake('a','e','i');
	printf("TUKAJ\n");
	int n;
	n = produktDvehVsot(1,2,3,4);
	printf("Produkt dveh vsot je : %d\n", n );
	printf("Produkt dveh vsot je : %d\n", produktDvehVsot(1,2,3,4) );
	printf("Preverjamo deljivost z 10 stevila 27:\n");
	aliJeDeljivoZDeset(27);
	printf("Skupna dolzina nizov:\n");
	char niz1[256] = "abcdef";
	char niz2[256] = "ghijklmno";
	skupnaDolzinaDvehNizov(niz1, niz2);
	skupnaDolzinaDvehNizov("test", "spettest");
	return 0;
}
/*
 * Sestavite funkcijo, ki vrne produkt dveh vsot (a+b)*(c+d)
 * Sestavite funkcijo, ki izpise, ali je stevilo deljivo z 10
 * Sestavite funkcijo, ki izpise skupno dolzino 2 
 * nizov, ki jih prejme kot argument. Uporabite strlen
 * 
 * Sestavite funkcijo, ki vrne produkt 3 stevil
 * ce dobi a, b, c in d kot argumente
 * Sestavite funkcijo, ki na zaslon izpise, ali je stevilo sodo.
 * Sestavite funkcijo, ki izpise tabelo 5 stevil, 
 * ki jo dobi kot argument
 * Sestavite funkcijo, ki vrne vsoto elementov tabele 5 stevil
 * Sestavite funkcijo, ki vrne povprecje
	( realno stevilo) elementov tabele 5 stevil
*/
