#include <stdio.h>

/* Domaca naloga : 
 * Sestavite program, ki prikazuje igralno polje za 4-v-vrsto, in omogoca igranje 
 * kot smo danes ze naredili za krizce in krozce.
 * Izpis igralnega polja naj bo narejen v funkciji.
 * 
 * Bonus naloga : 
 * program naj pove, ce je kje v igralnem polju zasledil 4 v eni vrsti. Stolpcev in diagonal ni potrebno testirati. 
 * Bonus 2 : enako se za stolpce in diagonale
 */
 
int main(int argc, char **argv)
{
	char igralnoPolje[6][7];
	int i, j; // Zacnemo s praznim poljem :
	for( i=0; i<6; ++i )
		for( j=0; j<7; ++j )
			igralnoPolje[i][j] = '_'; // Presledek
	int vrstica=0, stolpec=0;
	char XaliO = 'X';
	while( 1 )
	{
		// Izpisi igralno polje : 
		printf("Igralno polje je : \n");
		for( i=0; i<6; ++i )
		{
			for( j=0; j<7; ++j )
				printf("%c ", igralnoPolje[i][j] );
			printf("\n");
		}
		// preberi od uporabnika stolpec, in nastavi ustrezni element tabele na 'X' ali 'O'
		printf("Vnesite stolpec : ");
		scanf("%d", &stolpec );
		if( stolpec >= 7 ||stolpec < 0 )
			break;
		// Najdemo najvisjo vrstico v tem stolpcu, kjer je prazno polje
		for( vrstica=6-1; vrstica >= 0; --vrstica )
			if( igralnoPolje[vrstica][stolpec] == '_' )
				break;
		// Preverimo, ali ni mogoce stolpec poln :
		if( vrstica < 0 )
		{
			printf("Stolpec %d je poln, poizkusi ponovno\n", stolpec );
			continue;
		}
		igralnoPolje[vrstica][stolpec] = XaliO;
		if( XaliO == 'X' )
			XaliO = 'O';
		else
			XaliO = 'X';
		// Bonus : ali je v vrstici "vrstica" 3 X ?
		/* int stevilo_x, stevilo_o;
		for( i=0; i<3; ++i )
			if( igralnoPolje[i][0] == 'X' )
				stevilo_x++; */
	}
	/*
	 * Bonus naloga : 
	 * Sestavi enak program za 4-v-vrsto, ki ima igralno polje 5x7, 
	 * in vsak zeton pade do konca na tla.
	 * 
	 * Bonus naloga : 
	 * program naj pri obeh igrah preverja, ali se je igra ze koncala.
	 */
	return 0;
}
