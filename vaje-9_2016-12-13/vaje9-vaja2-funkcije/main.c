#include <stdio.h>
float kvocientDvehStevil( float stevilo1, float ); // Deklaracija
/* Funkcije : ponavljanje 
float kvocientDvehStevil( float stevilo1, float stevilo2 )
(tip, ki ga vrne) imeFunkcije ( <tip 1. argumenta> <ime 1. argumenta>, <tip 2. argumenta> <ime 2. argumenta> )
*/
float kvocientDvehStevil( float stevilo1, float stevilo2 )
{
	return stevilo1 / stevilo2;
}
// Naloge : 
// ## Napisite funkcijo, ki izracuna vrednost polinoma 2. stopnje v dani tocki. 
// Polinom : y = a0 + a1*x + a2*x^2
// Funkciji podamo tocko in koeficiente polinoma.
float povejVrednostPolinoma( float x, float, float, float a2 ); // Deklaracija
float povejVrednostPolinoma( float x, float a0, float a1, float a2 )
{
	float y;
	y = a0 + a1*x + a2*x*x;
	return y;
}
// ## Napisite funkcijo, ki izpise kvadrate vseh stevil do danega celega stevila.
void izpisiKvadrate( int n )
{
	int i = 1;
	while( i*i <= n )
	{
		printf("%d ", i*i );
		i = i+1;
	}
	printf("\n");
}
// ## Napisite funkcijo, ki izracuna fakulteto stevila z rekurzijo.
// n! = n * (n-1) * (n-2) * ... * 3 * 2 * 1
// n! = n * (n-1)!
int fakulteta( int n )
{
	int vrednost;
	printf("# klicem fakulteta ( %d )\n", n );
	if( n == 1 )
		vrednost = 1;
	else
		vrednost = n * fakulteta(n-1);
	printf("# vracam fakulteta ( %d ) = %d\n", n, vrednost );
	return vrednost;
}
// Napisite funkcijo, ki izracuna vrednost polinoma 3 stopnje (podan s koef a0..a3) v dani tocki.
float vrednostPolinoma3( float x, float a0, float a1, float a2, float a3 )
{
	return a0 + a1*x + a2*x*x + a3*x*x*x;
}
// Napisite funkcijo, ki izpise igralno polje za 4 v vrsto, podano kot tabela
void izpisiIgralnoPolje( char igralnoPolje[6][7] )
{
	printf("Igralno polje je : \n");
	int i,j;
	for( i=0; i<6; ++i )
	{
		for( j=0; j<7; ++j )
			printf("%c ", igralnoPolje[i][j] );
		printf("\n");
	}
}
// Napisite funkcijo, ki izracuna vrednost polinoma 4 stopnje (podan s koef a0..a4) v dani tocki.
// ## Napisite funkcijo, ki vrne najmanjso niclo polinoma 2. stopnje (podan s koeficienti)
#include <math.h>
float najmanjsaNiclaPolinomaDrugeStopnje( float a, float b, float c )
{
	float D;
	D = b*b - 4 * a * c;
	if( D < 0 )
	{
		printf("Nicla polinoma ne obstaja!\n");
		return 0;
	}
	return ( - b - sqrt(D) ) / ( 2 * a );
}
// Napisite funkcijo, ki izpise igralno polje za krizce in krozce, podano kot tabela
// ## Napisite funkcijo, ki izracuna n-to fibonacijevo stevilo. 
int fibonacijevaStevila( int n )
{
	if( n == 0 || n == 1 )
		return 1;
	else 
		return fibonacijevaStevila(n-1) + fibonacijevaStevila(n-2);
}
// f(0) = f(1) = 1
// f(n+1) = f(n) + f(n-1), 
// ## Napisite funkcijo, ki izracuna razdaljo med dvema tockama v ravnini, 
// podanima kot x1,y1,x2,y2.
float razdaljaMedDvemaTockama( float x1, float y1, float x2, float y2 )
{
	float dx = x1-x2;
	float dy = y1-y2;
	float d = sqrt( dx*dx + dy*dy );
	return d;
}
// Napisite funkcijo, ki vrne produkt 5 celih stevil
// ## Napisite funkcijo, ki vrne najvecji skupni deljitelj 2 stevil
int najvecjiSkupniDeljiteljDvehStevil( int a, int b )
{
	int i, deljitelj=1;
	for( i=1; i<=a; ++i )
	{
		if( a%i == 0 && b%i == 0 )
			deljitelj = i;
	}
	return deljitelj;
}

// Napisite funkcijo, ki vrne najmanjsi skupni veckratnik dveh stevil
int main(int argc, char **argv)
{
	printf("Kvocient %f in %f je %f.\n", 15.0, 10.0, kvocientDvehStevil(15.0, 10.0) );
	printf("Izpisujemo kvadrate do danega stevila\n");
	izpisiKvadrate(81);
	izpisiKvadrate(35);
	printf("Fakulteta 8\n");
	printf("%d ! = %d\n", 8, fakulteta(8) );
	char igralnoPolje[6][7];
	int i, j; // Zacnemo s praznim poljem :
	for( i=0; i<6; ++i )
		for( j=0; j<7; ++j )
			igralnoPolje[i][j] = '_'; // Presledek
	igralnoPolje[3][3] = 'X';
	igralnoPolje[4][4] = 'O';
	izpisiIgralnoPolje(igralnoPolje);
	
	printf("Nicla polinoma 1x^2 + 4*x + 4 je %f\n", najmanjsaNiclaPolinomaDrugeStopnje(1,4,4) );

	printf("F( %d ) = %d\n", 20, fibonacijevaStevila(20) );
	printf("F( %d ) = %d\n", 5, fibonacijevaStevila(5) );
	
	printf("Najvecji skupni deljitelj %d in %d je %d.\n", 
			72, 54, najvecjiSkupniDeljiteljDvehStevil(72, 54 ) );
	printf("Najvecji skupni deljitelj %d in %d je %d.\n", 
			256, 1024, najvecjiSkupniDeljiteljDvehStevil(256, 1024 ) );			
	return 0;
}

