#include <stdio.h>

	/* Naloge : 
	 * Naredite funkcijo, ki vrne najvecje in najmanjse stevilo v 
	 * tabeli in ju vrne preko kazalca.
	 * Naredite funkcijo, ki izpise tabelo na vseh mestih med dvema indeksoma
	 * Naredite funkcijo, ki zamenja dve celi stevili
	 * */
void najmanjsiInNajvecji( int tabela[5], 
	int * najmanjsi, 
	int * najvecji )
{
	int i;
	*najmanjsi = tabela[0];
	*najvecji = tabela[0];
	for( i=0; i<5; ++i )
	{
		if( tabela[i] > *najvecji )
			*najvecji = tabela[i];
		if( tabela[i] < *najmanjsi )
			*najmanjsi = tabela[i];		
	}
}
void najmanjsiInNajvecjiSSPREMENLJIVKAMI( int tabela[5], 
	int najmanjsi, 
	int najvecji )
{
	int i;
	najmanjsi = tabela[0];
	najvecji = tabela[1];
	for( i=0; i<5; ++i )
	{
		if( tabela[i] > najvecji )
			najvecji = tabela[i];
		if( tabela[i] < najmanjsi )
			najmanjsi = tabela[i];		
	}
}

// 	 * Naredite funkcijo, ki izpise tabelo na vseh mestih med dvema indeksoma
void izpisiTabeloMedIndeksi( int * tabela, int i, int j )
{
	for( ; i<j; ++i )
		printf("%d ", tabela[i] );
	printf("\n");
}
// 	 * Naredite funkcijo, ki zamenja dve celi stevili
void zamenjajDveStevili( int * a, int * b )
{
	int c = *a; 
	*a = *b;
	*b = c;
}
void zamenjajDveSteviliSSPREMENLJIVKAMI( int a, int b )
{
	int c = a; 
	a = b;
	b = c;
}
 /* Naredite funkcijo, ki izpise tabelo poljubne dolzine. 
* Dolzino tabele podamo kot argument.
* 
* Naredite funkcijo, ki napolni tabelo z zaporednimi stevili.
*/

int main(int argc, char **argv)
{
	/* Kazalci
	 * 
	 * V programskem jeziku C so kazalci posebne spremenljivke, 
	 * ki nam povedo, kje v spominu je zapisana vrednost neke spremenljivke. 
	 * Na primer :
	 */
	int stevilo = 6; // Spremenljivka 
	int spremenljivka = stevilo;
	int * kazalecNaStevilo = & stevilo; // Kazalec, ki kaze na kraj, kjer je vrednost spremenljivke
	// (*) pomeni, da je neka spremenljivka kazalec na spremenljivko dolocenega tipa
	//                      (&) pomeni povej naslov neke spremenljivke
	printf("Prvotna vrednost : \n");
	printf("Stevilo : %d\n", stevilo );
	printf("Spremenljivka : %d\n", spremenljivka );
	printf("Preko kazalca : %d\n", *kazalecNaStevilo );
	//                            (*) pred kazalcem pomeni "Poglej na ta naslov in povej kaj je tam"
	//                                in pretvori kazalec v spremenljivko
	// Ko spremenimo spremenljivko, se spremeni tudi vrednost, 
	// ki jo preberemo preko kazalca, saj gre v obeh primerih za eno in isto spremenljivko : 
	stevilo = 10;
	printf("Po spremembi preko stevilke : \n");
	printf("Stevilo : %d\n", stevilo );
	printf("Spremenljivka : %d\n", spremenljivka );
	printf("Preko kazalca : %d\n", *kazalecNaStevilo );
	// Vrednost spremenljivke lahko spremenimo tudi preko kazalca : 
	*kazalecNaStevilo = 20;
	printf("Po spremembi preko kazalca : \n");
	printf("Stevilo : %d\n", stevilo );
	printf("Preko kazalca : %d\n", *kazalecNaStevilo );
	// Vrednost spremenljivke pod kazalcem lahko nastavimo tudi preko branja od uporabnika : 
	scanf("Vnesite novo vrednost, kazalca : ", kazalecNaStevilo );
	//                                       ( ) ! TU NI & ko uporabimo kazalec
	printf("Po branju od uproabnika : \n");
	printf("Stevilo : %d\n", stevilo );
	printf("Preko kazalca : %d\n", *kazalecNaStevilo );
	
	/* Povzetek uporabe oznak (*), (&) : 
	int x;
	int * kaz_x = &x;
	
	*x : BREZ POMENA, a se prevajalnik lahko ne pritozi
	*kaz_x : pomeni poglej na naslov, kamor kaze kazalec in povej kaj je tam
	
	x : pomeni spremenljivko x
	kaz_x : pomeni kazalec x kot spremenljivko, in pove lokacijo v spominu, 
	        kamor kaze x
	*/
	
	/* Kazalci in tabele
	 * 
	 * V programskem jeziku C je tabela predstavljna s kazalcem na prvi element tabele : 
	 */
	int tabela[3] = { 1, 2, 3 };
	printf("Preko kazalca na tabelo : %d\n", *tabela );
	printf("Vrednost tabela : %d\n", (unsigned int)tabela );
	printf("Lokacija v spominu tabela[0] : %d\n", &tabela[0] );
	
	 /* 
	 * Naredite funkcijo, ki izpise tabelo poljubne dolzine. Dolzino tabele podamo kot argument.
	 * Naredite funkcijo, ki napolni tabelo z zaporednimi stevili.
	 * Naredite funkcijo, ki pretvori vse male crke v nizu v velike crke. AbcdEfg -> ABCDEFG
	 * Naredite funkcijo, ki najde dve zaporedni stevili v tabeli, ki nista urejeni po vrsti in ju zamenja.
	 */
	
	int novaTabela[5] = { 1, 15, -22, 77, 10 };
	int najvecji, najmanjsi;
	najmanjsiInNajvecji( novaTabela, &najmanjsi, &najvecji );
	printf("Najvecja in najmanjsa vrednost sta : %d, %d\n", najvecji, najmanjsi );
	najvecji = 0; najmanjsi = 0;
	najmanjsiInNajvecjiSSPREMENLJIVKAMI( novaTabela, najmanjsi, najvecji );
	printf("Najvecja in najmanjsa vrednost sta : %d, %d\n", najvecji, najmanjsi );

	printf("\n\n\n");
	printf("Izpisujem tabelo med 2 in 4\n");
	izpisiTabeloMedIndeksi(novaTabela, 2, 4 );
	printf("Izpisujem tabelo med 2 in 10\n");
	izpisiTabeloMedIndeksi(novaTabela, 2, 10 );
	
	int a = 10;
	int b = 20;
	printf("\n\n\n");
	printf("a = %d, b = %d\n", a, b );
	zamenjajDveStevili(&a, &b );
	printf("a = %d, b = %d\n", a, b );
	zamenjajDveStevili(&a, &b );

	printf("a = %d, b = %d\n", a, b );
	zamenjajDveSteviliSSPREMENLJIVKAMI(a, b );
	printf("a = %d, b = %d\n", a, b );

	return 0;
}
